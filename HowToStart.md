Start

1.	Fork the repository. Follow the Bitbucket documentation [here](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html)

		a.	Use the fork button that is hidden in the (+) symbol on the left and at the 
			bottom should say Fork Repository.
		b.	The format that others have used is to name it AutomatedMedia + yourname 
			so it is easier to 
			identify for pulls. You can also customize it and make it whatever you want,
			but make sure your team mates know what you named it, so they wont mistake you 
			for some other person.

2.	Clone the repository to your local hard drive. Follow Bitbucket documentation [here](https://confluence.atlassian.com/bitbucket/copy-your-git-repository-and-add-files-746520876.html)

		a.	Click the clone button near the top of the right inside the repository 
			you want to clone.
		b.	Copy the link.
		c.	Paste the link in a directory on your hard drive that does not have another 
			repository in it.
		d.	You can also follow the steps linked to help you more

3.	Once the repository is copied. Set your upstream to the main repository.

		a.	On the bitbucket page, copy the string found in the top right corner of the 
			repos Overview page.
		b.	Go to a command line prompt and in the directory with the repository type
		c.	git remote add upstream string you just copied
		d.	This will be where you pull the latest dev branch.

4.	Once you get the repository, you are ready to start working and contributing to the 
	project.

