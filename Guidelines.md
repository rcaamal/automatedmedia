Guidelines

Code

### C#

•Use standard C# naming conventions as shown [here](https://www.dofactory.com/reference/csharp-coding-standards)

•	Use RESTful style when creating controllers (i.e., a new controller for differentiable entities)

•	Use XML comments for all public methods
	
### JavaScript

•	Use external script files
	
Style

•Use external CSS files (no in-line CSS)

•Fonts used for this site are as follows:

	•h1, h2, h3 (size varies)
	•h4, body, p (size varies)
	•If using a templet, make sure everything matches
	•Add a link if using wireframe as your layouts
	
### Database
•Pluralize table names

•Table primary keys are <TableName>ID

•Foreign keys are <Entity>ID

•Make sure we have all the data we need.
	
### Bitbucket
•Use branches

•Commit often (dont feel like you have to have made major, complete, changes or new features before committing)

•Write good commit messages

•Dont commit code that doesnt compile

•It is okay to work on a separate testing file in your local repository in order to learn something, but dont keep multiple copies of a real file around just to keep some commented out pieces of code. As long as you have committed often you can always go back anywhere in your history to see what it looked like

•Dont add and commit any files that are auto-generated (i.e. html documentation, o, .tmp, ...)

•If you are working with your team, make sure to figure out conflicts before merging or your pull request will be denied.

•Make sure you are using your forked copy of the repository (if you have access to the main one) and that you are using a feature branch so when sending the commits and pull request complications wont occur.

### Pull Request Model
1.	Before doing any coding, make sure you have fetched upstream everything that has been updated and ready to be interrogated in your own repository.
2.	Then make sure you have pulled upstream the latest changes from the upstream remote repository to your forked repository in both the master and dev branches. Push these changes to your remote repository.
3.	Create your feature branches off of the develop branch and do all your development there. Commit often.
4.	When you have finished you work, you are read to merge it into the main Develop branch, but first do the following:
	a.	Fetch any changes that have occurred since you branched.
	b.	Checkout the develop branch and pull the new items that have been added.
	c.	Checkout your feature branch and merge develop into your branch.
	d.	Fix any merge conflicts.
	e.	Test thoroughly by building and running the project, making sure everything still looks and works the way you intended.
5.	Push your feature branch to your remote repository.
6.	From your forked repository on [Bitbucket](https://bitbucket.org/dashboard/overview), create a new [pull request](https://www.atlassian.com/git/tutorials/making-a-pull-request):
	a.	Ensure that the branch you are merging from is your feature branch on your forked repository.
	b.	Ensure that the branch you are merging into is the develop branch on the upstream repository (you will likely need to change this from master to develop).
	c.	Fill out the title and description fields as necessary.
	d.	If you are no longer going to be working on this feature branch, feel free to check the box to close the branch after the pull request is merged.
	e.	Create the pull request.
7.	Once your pull request is accepted and merged, pull the updates to your local develop branch and push to your remote repository.
