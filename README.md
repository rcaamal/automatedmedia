This is the repository for the team Automated Media for the CS461/CS462 Senior Project 2019 at Western Oregon University. The main project in this course is meant as a capstone project for a bachelor's degree in Computer Science.

### The contents of this repository include:

1.	BitbucketAssistant

    a.	This is an assistant on using bitbucket
	
2.	Milestones

    a.	Milestone 1
    
        I.	Team name
        II.	Team motto
        III.	Team logo
        IV.	Team letterhead
        V.	Team business cards
        VI.	Resume for each team member
        VII.	Workbook skeleton
        VIII.	Team schedule, including meeting time with advisor Slack team
        IX.	Git repository for team project
        X.	Three team project ideas
		
    b.	Milestone 2
    
        I.	Class Project:
            1.	Lists of needs & features
            2.	Lists of requirements, including non-functional
            3.	Output of initial modeling
            4.	Initial architecture
            5.	User stories
            6.	Full vision statement

    c.	Milestone 3
    
        I.	Class Project:
            1.	Epics/Features/User stories in Azure DevOps, each with ID, effort, priority and description
            2.	Commit one user story per person, run 1 mini-iteration (sprint) to
            3.	Confirm architecture: minimal introductory site deployed on Azure, with database interaction,database deployed on Azure
            4.	Go through all Scrum meetings
            5.	Team Project Inception (Part 1)
            6.	Mind map or other brainstorming output
            7.	Vision description
            8.	Preliminary needs and features list
            
    d.	Milestone 4
    
        I.	Class Project 1st Full Iteration:
            1.	All iteration artifacts updated in Azure DevOps, Git repository and result deployed with continuous deployment
        II.	Team Project Inception (Part 2):
            1.	Lists of needs and features
            2.	List of requirements, NFR's
            3.	Overall architecture design
            4.	Initial modeling (initial database design, use cases, ...) 
            5.	Timeline and release plan
            6.	Epics/Features/User stories in Azure DevOps backlog with ID, effort, priority.
            7.	Final Vision Statement
            8.	Identification of Risks
            
    e.	Milestone 5
    
        I.	Retrospectives
        
3.	AutomatedMediaFolder

    a.	Our project items.


# Automated Media
### Team Members:

Randy Caamal 

Xuan Jiang

Dakota Koki

Kaimeng Lyu



**Automated Media Project**

### Vision Statement

For people who can not remember a song and know how the song goes by humming it or by part of lyrics of a song, the sound record website is a site that will provide a search field to find songs on an artist. Many would have to search up the name of the song by typing it in or if they do not know the song name then the artist and have to listen to multiple songs to find the specific one, they are looking for. This would take time, and time is valuable to many. So many would want to simple and easy way to search music, perhaps by the lyrics of a song or even recorded audio. Shazam applies this type of software to help many users with that need which they have their API inside of Spotify's service database. Unlike this joint collaboration our application would give music and artist information provide a samples of the music. 


### How do I get started?

The [HowToStart](https://bitbucket.org/rcaamal/automatedmedia/src/master/HowToStart.md) file has information about how to clone and set up this project on your local machine and how to become a contributor.


### Contribution

For [contributers](https://bitbucket.org/rcaamal/automatedmedia/src/Develop/Contributors.md) and coding guildines when working on this project, here are the [guidelines](https://bitbucket.org/rcaamal/automatedmedia/src/Develop/Guidelines.md).
Here is a list of the contributors to this project.

### Software Construction Process

For this project, we are following Agile methodologies, specifically Disciplined Agile Delivery process framework.
We follow a two-week sprint cycle that involves daily SCRUM meetings, weekly sprint planning sessions, 
and an end of sprint review, retrospectives, and also a weekend group assignemnts to each group member, which is 
where we assign work to one another so they can work on the weekend if we are not going to meet.

### Team Song 
Here is the [team song](https://www.youtube.com/watch?v=ytQ5CYE1VZw)

### Tools, Configurations, and Packages

[Tools](https://bitbucket.org/rcaamal/automatedmedia/src/Develop/Tools.md) is where we kept a list of all of the software, libraries, tools, packages download by nugget, and versions used
for our project. We made sure our team members were using the same version of everything to avoid conflictions. 
So, make sure you are using the same ones to avoid any compatibility errors, if you are using a newer version use these as reference 
and then search for the current version because it might give you errors for using a different version then these.
