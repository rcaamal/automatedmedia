## Initial Requirements Elaboration and Elicitation

### Questions

1. How do we link a discussion on the site to one or more articles/pages?

    We were thinking via URL.

2. How will users find out that there is a discussion on the site for the article/page they're currently viewing?
   
    How about a browser plug-in?  It could send the URL of the current page to our API to see if a discussion page exists and provide an easy way for them to navigate to our page.

    Or the user can copy the URL and paste it into a search bar on our site.

3. Clearly we need accounts and logins.  Our own, or do we allow logging in via 3rd party, i.e. "Log in with Google" or ...?  
    
   We allow users to logging in via 3rd party, but the best way is create an account on our own.

4. Do we allow people to comment anonymously?  Read anonymously? 

   
5. Do we allow people to sign up with a pseudonym or will we demand/enforce real names?
6. What is it important to know about our users?  What data should we collect?
7. If there are news articles on multiple sites that are about the same topic should we have separate discussion pages or just one?
8. What kind of discussion do we want to create? Linear traditional, chronological, ranked, or ?
9. Should we allow image/video uploads and host them ourselves?
10. What articles/ events are we going to display?
     Current events and articles.
11. Should we allow users to keep their transcript of the discussions?
     Yes, they can keep it if they want.
12. Should we set up a limitation of words in the discussions?
     The users might argue sometimes, so we will set a limitation of what they say to make sure they won't use rude words.  


### Interview Question### Interview Question
 1. There are a lot of websites just like yours',so what's the benefits for choosing your project?

    We will focus on a group of people who are intersted with international news, and we do provide a discussion area for them.

 2. Normally a website will focus on a group of people, like high school students, collage professors or some technicians, so what kind of group of people will be attracted?

    We will focus on the international news from worldwide, so that group will be focused on.

 3. If there is a big news coming out, many people are interested and discussed a lot, what's the best way to find some valuable comments for a manager?

    For valuable comments, we will make a filter to choose some comments which have more than 500 words. Also we will have a Like/Dislike button, the comments will have an order to show the popular comments.


