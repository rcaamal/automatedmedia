International Currency
1.What is new/original about this idea? What are related websites/apps?
   
   User can use this website inquire the real-time currency exchange rate of different country. There are many websites can get the information, but in our website users can change the currency online. They can make the transaction online and when they arrived airport then they can get money from the currency exchange station.

2.Why is this idea worth doing? Why is it useful and not boring?
   
   When I got to different countries, I find the currency station only receive cash to exchange money. It is not convenient for travelers to exchange money. I want to make this process more simple, so I want to make this website.

3.What resources will be required for you to complete this project that are not already included in the class. i.e. you already have the Microsoft stack, server, database so what else would you need? 
  
   For this website, we need the API from VISA Developer Center. They have the API of international currency rate. And we also need to solve the method of payment, contact the bank or make the transaction by ourselves.

4.What algorithmic content is there in this project?
   
   Statistical algorithm, Timestamp algorithim, Database algorithm.

5.Rate the topic with a difficulty rating of 1-10. 
   
   I think the difficulty is 8, the most difficult part is how to make the transaction. 