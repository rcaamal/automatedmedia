2018-19 Class Project Inception: Discussion Hub
Summary of Our Approach to Software Development
[What processes are we following? What are we choosing to do and at what level of detail or extent?]  

Initial Vision Discussion with Stakeholders
People like to talk; people like to discuss and argue. People like to state their opinions and read thoughts of others. But there are real problems with how it is happening on the Web. Just take a look at YouTube or Twitter comments. That's not a discussion and it's not often useful, productive or even civil. Social media is not the place to talk about things. The Internet should be a place where people can communicate. Let's make a site where people can do that!

Places like Reddit and Kialo already have a handle on general topic discussions, so we won't try to do that. Enthusiasts often have their own sites with discussion and comment sections (e.g. Slashdot) and have formed close-knit communities that work well. We don't want to do that either. Here's our idea, using an example:

Let's say I just read this article on CNN, American endurance athlete becomes the first person to cross Antarctica solo, about a guy from Oregon who skied across Antarctica. The thing is, is that he didn't and he wasn't the first. His claims should be challenged and the article/journalist is disingenuous by not at least bringing this up. People reading this article should know there are serious questions about it. Someone should comment on this to point it out and spark further discussion. There is no way to do this. CNN does not have a comment section. Even if it did, do I want to create an account there just so I can make a quick comment? I'd have to do that everywhere on the web where I had a question or wanted to comment. How would I know if my question was answered?

We want a centralized discussion site that can be found easily and where an individual can maintain an account, build a history, expertise, level of trust, etc. It should make it easy to create or find a discussion page about any news article, post or web site. It should provide features for the user to follow their discussions without ever going back to the original website. It should allow them to create and maintain their own identity that is separate from any social media identity.

Initial Requirements Elaboration and Elicitation
Questions
How do we link a discussion on the site to one or more articles/pages?

We were thinking via URL.

How will users find out that there is a discussion on the site for the article/page they're currently viewing?

How about a browser plug-in? It could send the URL of the current page to our API to see if a discussion page exists and provide an easy way for them to navigate to our page.

Or the user can copy the URL and paste it into a search bar on our site.

Clearly we need accounts and logins. Our own, or do we allow logging in via 3rd party, i.e. "Log in with Google" or ...?

Do we allow people to comment anonymously? Read anonymously?
Do we allow people to sign up with a pseudonym or will we demand/enforce real names?
What is it important to know about our users? What data should we collect?
If there are news articles on multiple sites that are about the same topic should we have separate discussion pages or just one?
What kind of discussion do we want to create? Linear traditional, chronological, ranked, or ?
Should we allow image/video uploads and host them ourselves?
Interviews
?
List of Needs and Features
A great looking landing page with info to tell the user what our site is all about and how to use it. Include a link to and a page with more info. Needs a page describing our company and our philosophy.
The ability to create a new discussion page about a given article/URL. This discussion page needs to allow users to write comments.
The ability to find a discussion page.
User accounts
A user needs to be able to keep track of things they've commented on and easily go back to those discussion pages. If someone rates or responds to their comment we need to alert them.
Allow users to identify fundamental questions and potential answers about the topic under discussion. Users can then vote on answers.
Initial Modeling
Use Case Diagrams
Other Modeling
Identify Non-Functional Requirements
User accounts and data must be stored indefinitely.
Site and data must be backed up regularly and have failover redundancy that will allow the site to remain functional in the event of loss of primary web server or primary database. We can live with 1 minute of complete downtime per event and up to 1 hour of read-only functionality before full capacity is restored.
Site should never return debug error pages. Web server must never return 404's. All server errors must be logged. Users should receive a custom error page in that case telling them what to do.
Must work in all languages and countries. English will be the default language but users can comment in their own language and we may translate it.
Identify Functional Requirements (User Stories)
E: Epic
F: Feature
U: User Story
T: Task

[U] As a visitor to the site I would like to see a fantastic and modern homepage that tells me how to use the site so I can decide if I want to use this service in the future.
[T] Create starter ASP dot NET MVC 5 Web Application with Individual User Accounts and no unit test project
[T] Switch it over to Bootstrap 4
[T] Create nice homepage: write content, customize navbar
[T] Create SQL Server database on Azure and configure web app to use it. Hide credentials.
[U] Fully enable Individual User Accounts
[T] Copy SQL schema from an existing ASP.NET Identity database and integrate it into our UP script
[T] Configure web app to use our db with Identity tables in it
[T] Create a user table and customize user pages to display additional data
[F] Allow logged in user to create new discussion page
[F] Allow any user to search for and find an existing discussion page
[E] Allow a logged in user to write a comment on an article in an existing discussion page
[F]
[U]
[T]
[T]
[T]
[U]
[F]
[F]
[U] As a robot I would like to be prevented from creating an account on your website so I don't ask millions of my friends to join your website and add comments about male enhancement drugs.
Initial Architecture Envisioning
Agile Data Modeling
Timeline and Release Plan