## Milestone 2
For the rubric of this milestone click here to take a look:
[Milestone 2](http://www.wou.edu/~morses/classes/cs46x/assignments/t2/M2.html)  
### Vision Statement
For people who use social media want to make group disscusion about a certain topic. The Disscussion Hub is a centralized website where people can create and discuss differnet topics. This will focus on managing the different topics that people come up with. It will allow comments to be more directed to the topic and not something off topic. This application would provide users with a more organized set of discussions and have a admin to monitor what people say in the discussions and commits. Unlike other social media sites like Reddit ours would manage all of the differnet discussions and if they see harsh or unwanted comments they will remove them from the discussion.

**User story/Functional Requriement**
[U1]As a visitor, I want easily to find all type of news so that might be helpful to find news which I liked.

[U2]As a logged in user, I want to discuss with my friends so I can have more interests in discussion.

[U3]As a user, I want to create discussion so that I can discuss what I am interested in.

[U4]As a vistior, I want to see the most discussed topics so that I can know which topics are hottest.

[U5]AS a vistior, I want to see the latest news and topices so that I can get the latest information.

[U6]As a user, I wnat to receive a reminder when someone replies to me so that I can discuss with others conveniently.

[U7]As a user, I want to like others comments so that I can express that I agree with someone.

[U8]As a user, I want to see others information so that I can better judge the reliability of the comments.

[U9]As a Administrators, I want to manage all comments so that I can manage website.

[U10]As a user, I want to set up the areas that I concerned so that I can focus on topics that interest to me.

[U11]AS a user, I want to search the news so I can easily get the information what I want.

**Interview Question**
[Q1]There are a lot of websites just like yours',so what's the benefits for choosing your project?

[Q2] Normally a website will focus on a group of people, like high school students, collage professors or some technicians, so what kind of group of people will be attracted?

[Q3]If there is a big news coming out, many people are interested and discussed a lot, what's the best way to find some valuable comments for a manager?  

### Non-functional Requirements
 1. User accounts and data must be stored indefinitely.
 2. Site and data must be backed up regularly and have failover redundancy that will allow the site to remain functional in the event of loss of primary web server or primary database. We can live with 1 minute of complete downtime per event and up to 1 hour of read-only functionality before full capacity is restored.
 3. Site should never return debug error pages. Web server must never return 404's. All server errors must be logged. Users should receive a custom error page in that case telling them what to do.
 4. Must work in all languages and countries. English will be the default language but users can comment in their own language and we may translate it.
 5. Links should not broken/misdirected.
 6. DB shold manage all incoming files/be up to date.
 7. Firewall design should screen out all files that are harmful, miscellaeous, viruses, etc...

### Needs and Features:

#### Features
[F1]Search Box
[F2]Create tab
[F3]Page of current topics
[F4]Report button
[F5]Like/Dislike button
[F6]Send invites to discussions
[F7]Create new discussion
[F8]personal information

#### Tasks
[T1]Report unwanted comments/discussions
[T2]Admin's need to monitor discussions
[T3]Validate comments

#### Epics
[E1]Allow user to create new topic/discussions
[E2]Allow admin to delet topics/discussions
[E3]Allow logged in users to add other logged in users
[E4]Add expertise and level of trust in the personal information
