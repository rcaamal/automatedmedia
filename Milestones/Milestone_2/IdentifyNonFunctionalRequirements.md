# IdentifyNonFunctionalRequirements
 1. User accounts and data must be stored indefinitely.
 2. Site and data must be backed up regularly and have failover redundancy that will allow the site to remain functional in the event of loss of primary web server or primary database. We can live with 1 minute of complete downtime per event and up to 1 hour of read-only functionality before full capacity is restored.
 3. Site should never return debug error pages. Web server must never return 404's. All server errors must be logged. Users should receive a custom error page in that case telling them what to do.
 4. Must work in all languages and countries. English will be the default language but users can comment in their own language and we may translate it.
 5. Links should not be broken/misdirected.
 6. DB should manage all incoming files/be up to date.
 7. Firewall design should screen out all files that are harmful, miscellaeous, viruses, etc...