Q1. What is new/original about this idea? What are related websites/apps? (Be able to answer the question: isn’t somebody already doing this?)

The new part about this is that we will be able to run the audio reconition to find a song but from there we will gather information on that song. If it was part of a movie and if so send us that information. Then we will allow users to rent out the movie or buy it. 

Q2. Why is this idea worth doing? Why is it useful and not boring?

When people search up music the main question is Who, what, when, and where. examples could be: where did I hear this song, where was this song from, etc... This way we can search up the information with out trying to remember the main idea of a movie or song. Also, it is a helpful way to get information about movies, games, and music, etc. This project is useful by allowing users not only search for movies and music, but it also has the ability to take a sound and voice recognition and search for what they are looking for. Also, users will be able to enjoy more movies, games, and songs faster than trying to think of what your friend is listening, playing, or even watching. 

Q3.What are a few major features?
The main feature is our voice audio recognition. 

Q4.What resources will be required for you to complete this project that are not already included in the class. i.e. you already have the Microsoft stack, server, database so what else would you need? Additional API’s, frameworks or platforms you’ll need to use.

The resources are the Spotify API and subscription API. Then for the Audio reconition we will use the AudD API and the Shazam API. 


Q5.What algorithmic content is there in this project? i.e. what algorithm(s) will you have to develop or implement in order to do something central to your project idea? (Remember, this isn’t just a software engineering course, it is your CS degree capstone course!)

We would need to use a audio Reconition algorithims and database to match the audio.

Q6.Rate the topic with a difficulty rating of 1-10. One being supremely easy to implement (not necessarily short though). Ten would require the best CS students using lots of what they learned in their CS degree, plus additional independent learning, to complete successfully.

The difficulty would be to this is a 7/10. The reason for this is that the Algorithm for the audio is already there where we can build somthing similar to it. Issues that may occur would be when we want to find a song from movie audio and making sure the audio is clear enough for the program to execute the search. 