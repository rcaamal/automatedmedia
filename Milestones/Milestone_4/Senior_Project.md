## Sound Record
Summary of our approach to software development

We are using the scrum process, and following the processes used in the Disciplined Agile Delivery book. 
 
### Initial Vision Discussion with Stakeholders 

For people looking up music is difficult on the internet. Many would have to search up the name of the song by typing it in or if they do not know the song name then the artist and have to listen to multiple songs to find the specific one they are looking for. This would take time and time is valuable to many. So many would want to simple and easy way to search music, perhaps by the lyrics of a song or even recorded audio. Shazam applies this type of software to help many users with that need which they have there API inside of Spotify’s service database. Unlike this joint collaboration our application would give music and artist information. I will also look into the song and see if this song is from a movie, tv show, or even possibly from a video game. This would give a person the thought of “this song was from this”. 

Currently, people are using shazam to search music and Spotify to play music. However, that is all they are being used for many still wonder “where did I here this from? “, Some would say they heard this from a movie, tv show, or video game. Shazam gives the song name and artist but sometimes they don’t mention if it was from something else. Then Spotify plays music but does not give the information of if the song was used in a movie, tv show, or video game and then you would have to search it out. 

A main thing that Spotify does not use is chat rooms where many people would use to talk to people and perhaps ask questions on a particular topic such as: what movie was this song from?, or does anyone know the name of this song?. Many would answer the question. Then users if they want more functionality they would need to be premium members which cost money.

Regional barriers are a big issue to many web services and applications. Sites do not usually have to have a language translator to be added into there software because many people can  read the specific language. Many people if there native language is different from the sites one they would try and find a different one to use.  Designing a video to show the functions of the website might be good for many users to understand how to use the site. 

We want a site where people can do searches not just by typing in the information such as artist name or song, but also able to find music with just audio itself. A simple website that people can have chats with others and ask questions about a particular song or artist. Then have it be simple in that users would not have a problem using the website that includes having a video to show how to use the application. 



#### Initial Requirements Elaboration and Elicitation
**Questions:** 
1.	What makes this project different from music search engine?
•	 It’s a overall use of both text input and audio recognition. 
2.	What will be importance of users in our web application?
•	It is just creating the username so we can ask questions to other users or just chat. 
3.	How will the audio be recognized in the project?
•	It will use lyrics from the music to match the audio or audio to audio recognition. 
4.	What type of API service would be used in the project? 
•	It will use Houndify API which is used in Pandora. 
5.	How will searches be different from searches done on other music hosting sites?
•	It will not only give the information of the artist of the song, it will also give information if the song used in any movie, tv show, or video game and give us that information. Otherwise it will just return the usual information. 
6.	What benefits do the site have by adding a chat box?
•	It can be used to create conversations between visitors
•	It can also be used ask and answer questions related to finding music or artist.

### Needs & Features (Requirements)

•	Have to well designed and appealing main page.
o	Which will hold a demo video of how to use or website.
•	Be able to create users
o	Create a username
•	Create chat rooms where people can talk and ask questions.
•	Search box to enter values:
o	to look up music
o	to look up artist
•	multiple language support
•	Voice Audio recognition using Houndify to search up music
o	Priority to make sure that recognition software works the way we want it to be. 
•	Navigation bar to have faster access to certain features. 
o	Homepage
o	Audio Recognition search area
o	History of recent searches
•	Have the history of what a visitor recently looked up
•	API that contains a list of all current music out there
o	Using the Spotify API

### Initial Modeling
 Use-case Diagrams

 Other modeling 






### Identify Non-functional requirements
•	The API database contains lyrics for songs
•	Site search must be correct to what exist in the database. If the search does not match anything currently in the database then we need to send the a error message to user saying “sorry did not find anything for that search.”
•	Works on all current browser services across different platforms such as:
o	Windows
o	Mac
o	Linux
•	Site must be able to handle large files usages such as playing music.
•	Audio recognition software works to do searches on a audio recordings and match that audio to songs currently in the database.
•	Should be able to work under different languages.
•	Separate database that holds all of usernames that were created from our website.
•	Check to see if our database holds all conversations, questions and search history.

### Identify Functional Requirements (user stories)
## Key: 
### [E] = Epic [F] = Feature [U] = User stories [T] = Tasks

1.	[E] Website functionality for visitors 
•	[F] Homepage should have a demo of how to use the site. 
	[U] As a visitor I want to see a well designed homepage with functionality
1.	[T] Create a starter ASP.NET MVC 5 web app with user accounts
a.	These accounts are to test to make sure the data is being fed into the SQL database. 
2.	[T] Make sure we are using bootstrap 4 and not a older version
3.	[T] Make sure the Homepage it well designed with images of:
a.	Music Artist pictures
b.	Games 
c.	Movies
d.	TV shows
	[U] As a visitor I want to able to understand how to use the website 
1.	 [T] Create a Demo video of how to us the website.
a.	Display the video on the main page. 
b.	Make sure that the video is not broken when added to the homepage.
•	[F] Navigation Bar
	[U] As a visitor/user I want to have a well design navigation bar to navigate around our site.
1.	[T] make action links to go to certain locations on the website such as:
a.	Search History 
b.	Homepage
c.	Create Username
•	[F] Creating users
	[U] As users they can create a username
	[U] As users I want to keep my information private/secured.
	[U] As new users I want to be welcomed to the website.
•	[F] “Deleting Account”
	[U] As a user I want to remove an account when I no longer want it.
1.	[T] Flag down user account when it is requested to be deleted
2.	[T] Create a request form to delete account. 
2.	 [E] Search Engine tools
•	[F] Text Search 
	[U] As a user I want to do a search on a song or artist by just putting in text. 
1.	[T] Make a search box to able to handle text input values.
a.	This will handle (nvarchar) values
2.	[T] make a Javascript file to run our search through the API database.
3.	[T] Make a submit button to execute the search.
•	[F] Audio Search
	[U] As a user I want to do a search by using audio recordings. 
1.	[T] Create a play/search button to listen to the recording.
2.	[T] Create a javascript to run our audio search through our API
a.	This will return (nvarchar) values.
3.	[T] A redirect to the end result of the search.
3.	[E] conversations between users
•	[F] chat box
	[U] As a user I want to make some for conversation between users. 


Initial Architecture Envisioning

Agile Data Modeling




### Timeline and Release plan 
	Sprint 1  Start: Wednesday Feb 13th – End: Tuesday Feb 26th: Release 1  
	Sprint 2 Start: Wednesday Feb 27th – End: Tuesday March 12th: Release 2
	Sprint 3 Start: Wednesday April 3rd – End: Tuesday April 16th  : Release 3  
	Sprint 4 Start: Wednesday April 17th – End: Tuesday April 30th : Release 4
	Sprint 5 Start: Wednesday May 1st – End: Tuesday May 14th : Release 5
	Test: TBD
	Sprint 6 Start Wednesday 15th  - End: Tuesday May 28th: Release 6 

### Identification of Risk
	If one of us does not pass CS 461
	The API’s we use are not free to access and perhaps have some legal contract requirements in order to use it.
	The Main repository can is in disarray and can no longer be used.
	API services shutdown suddenly
o	Crash of server
o	API database does not exist anymore
	Voice Recognition API is not functioning
	Entire Web server crash











