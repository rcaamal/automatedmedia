
CREATE TABLE [dbo].[Users]
(
	[UserID]			  INT IDENTITY (1,1)	NOT NULL,
	[Username]		      NVARCHAR(64)		    NOT NULL,
	[Password]		      NVARCHAR(64)		    NOT NULL,
	CONSTRAINT [PK_dbo.Users] PRIMARY KEY CLUSTERED ([UserID] ASC)
);

CREATE TABLE [dbo].[Topics]
(
	[TopicID]			  INT IDENTITY (1,1)	NOT NULL,
	[TopicTitle]		  NVARCHAR(64)		    NOT NULL,
	[TopicsDescription]   NVARCHAR (150)         NOT NULL,
	CONSTRAINT [PK_dbo.Topics] PRIMARY KEY CLUSTERED ([TopicID] ASC)
);

CREATE TABLE [dbo].[Comments]
(
	CommentID			INT IDENTITY (1,1)      NOT NULL,
	CommentTitle		NVARCHAR(80)	        NOT NULL,
	CommentTime         DateTime           	NOT NULL,
	CONSTRAINT [PK_dbo.Comments] PRIMARY KEY CLUSTERED (CommentID ASC),
);

CREATE TABLE [dbo].[DiscussionBoards]
(
	DiscussionBoardID			INT IDENTITY (1,1)     NOT NULL,
	DiscussionTitle     		NVARCHAR(150)	       NOT NULL,
	DiscussionDescription       NVARCHAR(300)	       NOT NULL,
	TimePost            		DateTime 			   NOT NULL,
	UserID                      INT                    NOT NULL,
	TopicID                     INT                    NOT NULL,
	CommentID                   INT                    NOT NULL,
	CONSTRAINT [PK_dbo.DiscussionBoards] PRIMARY KEY CLUSTERED (DiscussionBoardID ASC),
	CONSTRAINT [FK_dbo.DiscussionBoards] FOREIGN KEY (UserID) REFERENCES [dbo].[Users] (UserID),
	CONSTRAINT [FK2_dbo.DiscussionBoards] FOREIGN KEY (TopicID) REFERENCES [dbo].[Topics] (TopicID),
	CONSTRAINT [FK3_dbo.DiscussionBoards] FOREIGN KEY (CommentID) REFERENCES [dbo].[Comments] (CommentID),
);

INSERT INTO dbo.Users (Username,Password) VALUES
('Jane Stone','000000'),
('Tom McMasters','111111'),
('Otto Vanderwall','222222');

INSERT INTO dbo.Topics (TopicTitle,TopicsDescription) VALUES
('Hello World','Just saying'),
('WOU','Tell me about your experience at WOU'),
('Happy','Are you happy?');

INSERT INTO dbo.Comments (CommentTitle,CommentTime) VALUES
('AHAHAHA','12/04/2017 09:04:22'),
('HIhihihi','12/04/2017 04:04:22'),
('Ummmm','12/04/2017 05:04:22');

INSERT INTO dbo.DiscussionBoards (DiscussionTitle,DiscussionDescription,TimePost,UserID,TopicID,CommentID) VALUES
('AHAHAHA','aaaa','12/04/2011 09:04:22',1,1,1),
('HIhihihi','ssss','12/04/2013 04:04:22',2,2,2),
('Ummmm','xxxx','12/04/2013 05:04:22',3,3,3);