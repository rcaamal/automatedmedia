namespace MilesoneThree.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DiscussionBoard
    {
        public int DiscussionBoardID { get; set; }

        [Required]
        [StringLength(150)]
        public string DiscussionTitle { get; set; }

        [Required]
        [StringLength(300)]
        public string DiscussionDescription { get; set; }

        public DateTime TimePost { get; set; }

        public int UserID { get; set; }

        public int TopicID { get; set; }

        public int CommentID { get; set; }

        public virtual Comment Comment { get; set; }

        public virtual User User { get; set; }

        public virtual Topic Topic { get; set; }
    }
}
