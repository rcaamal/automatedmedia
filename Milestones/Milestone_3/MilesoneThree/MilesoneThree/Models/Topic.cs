namespace MilesoneThree.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Topic
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Topic()
        {
            DiscussionBoards = new HashSet<DiscussionBoard>();
        }

        public int TopicID { get; set; }

        [Required]
        [StringLength(64)]
        public string TopicTitle { get; set; }

        [Required]
        [StringLength(150)]
        public string TopicsDescription { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DiscussionBoard> DiscussionBoards { get; set; }
    }
}
