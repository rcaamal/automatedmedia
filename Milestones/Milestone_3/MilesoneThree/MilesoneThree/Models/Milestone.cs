namespace MilesoneThree.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Milestone : DbContext
    {
        public Milestone()
            : base("name=Milestone")
        {
        }

        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<DiscussionBoard> DiscussionBoards { get; set; }
        public virtual DbSet<Topic> Topics { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Comment>()
                .HasMany(e => e.DiscussionBoards)
                .WithRequired(e => e.Comment)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Topic>()
                .HasMany(e => e.DiscussionBoards)
                .WithRequired(e => e.Topic)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.DiscussionBoards)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);
        }
    }
}
