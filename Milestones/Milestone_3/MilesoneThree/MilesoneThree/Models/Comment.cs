namespace MilesoneThree.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Comment
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Comment()
        {
            DiscussionBoards = new HashSet<DiscussionBoard>();
        }

        public int CommentID { get; set; }

        [Required]
        [StringLength(80)]
        public string CommentTitle { get; set; }

        public DateTime CommentTime { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DiscussionBoard> DiscussionBoards { get; set; }
    }
}
