## Milestone 1

Primary Objectives:

Form your development team
Learn to work as a team
Create team documents with a common format
Propose 3 possible projects


Overall Requirements:

Team name
Team motto
Team logo
Team letterhead
Team business cards
Résumé for each team member
Workbook skeleton
Team schedule, including meeting time with advisor
Slack team
Git repository for team project
Three team project ideas
Grade sheet (pdf)
___________________________________________________________________________________________________________________________________________
Introduction

This milestone will get your team organized and begin the process of working together as a group.
Don't underestimate the difficulty of doing simple things as a group. Communicate, communicate, communicate!

In step 6 you will create a Git repository for your team. This is where all the documents created during each Milestone should be placed
(neatly and in appropriate folders). A note on file formats: you should default to plain text files for everything. The resumes (e.g. Word)
and logos, etc. (e.g. png) can and should be in the appropriate formats, but if you're only writing text, put it in a Plain Old Text file.
 Of course, html and Markdown are plain text and also give you formatting options if you so desire.

The 3+branch, with Pull Requests Git Workflow that we'll be using all term isn't introduced until Milestone 2.
For now you can either learn the workflow ahead of time, or do the less-than-desired method of manually transferring files to the repo owner,
who should add/commit/push them. Under no circumstances should all team members have admin or write access to the team repository.
That's a great way to screw up your repo. Last term we all had our own repositories and could control the entire push/pull cycles.
Imagine 4 people randomly adding, committing, pushing and pulling!

Tasks:

Select a team name. This is how your team will be identified for the next 22 weeks, so give it some thought.
Also create a team motto, something short like “Success through engineering”. You will need to create a logo and a letterhead for your team.
Also prepare business cards for each member.
This section of the milestone will include a resume for each member of the team.
All of the resumes should look the same and contain the same major headings, and have the same format including font size and type,
and order of sections.

The Rockport Institute contains some good information for writing resumes. For the resume in this section,
your resumes must conform to the combinatorial type described in the website. It must contain the sections outlined below.
Resumes that are not consistent with each other and the required format will not be considered completed and will receive 0 completion points.

As you will see when you search the web, there are three basic resume types: Chronological, Functional, Combinational

For this class we will be using a version of the Combinational Resume.
Your resume should be no longer than one page and contain the following sections.
As pointed out in the Rockport site, the resume is advertising copy whose purpose is to obtain an interview.

Your resume for this class should contain

Name and Contact Information
Career Objective (Writing an Objective)
Summary of qualifications section (Writing a Sumary Section)
Skills Section

A listing of various computer science related skills such as Software Engineering, Operating Systems.
Keep in mind that probably every CS graduate in the country knows how to use Microsoft Windows and program in Java.
You need to focus on those skills that make you unique. In looking at the Rockport site focus on the way in which “selected skills and
accomplishments” are written. You might consider naming sections after courses you have taken in your program.
The use of strong active verbs is important. It is ok if these sections make the resume exceed one page. When applying for a particular job,
you can remove some of the sections to make it fit within one page.

Employment History and Education

These are two separate sections. I mention them together because the ordering depends on which you have more of.
If you are a returning student who has managerial experience, definitely put the employment history first.
If you went straight to college from high school, probably the Education should go first.

Both of these sections should be listed in “reverse chronological order”, that is most recent first.
You would list your college graduation before high school.

Most of the things that would be listed as part of your experiences in a chronological resume have been moved up into the skills section and
need not be repeated here. This section basically becomes a chronological list.

One last note on Education, if your GPA is in the top 10% list it, otherwise leave it out.
A neat trick with GPA is to calculate only classes within the major.

Acquire and assemble a three ring binder with dividers for each work product

Scrum Reference Card: place a copy in the front of your notebook
Have Tab separators for each Milestone
Each Milestone section must contain (in this order)
Milestone grade sheet (on top and completely filled out before your meeting)
A printout of the milestone (This avoids confusion during meetings.)
Work products IN THE ORDER THEY APPEAR IN THE MILESTONE. Include items that can be printed out (e.g. logo, resumes, business cards, ...
for this milestone)
Create a schedule for your team that lists five hours of meetings during the week. Although you will not use all of them,
it is good to know they are available during “crunch time”.
There are several specific meetings described in the SCRUM tutorials THAT ARE REQUIRED!
Define some of your schedule times for the five defined SCRUM meetings. Standup meetings occur at the beginning of each class, and
the scheduled meeting with your team instructor can function as the sprint review meeting.
Schedule time for the remaining meetings into your team’s weekly schedule. See the SCRUM reference card for a quick overview of the meetings.

As just mentioned, you will need to schedule a fixed time to meet with your project advisor during the week.
As in the real world, you will need to find a time that is convenient for them. Not one that works for your schedule.
Your best bets are to look at times that match already scheduled office hours or are next to existing courses.
Do not expect a professor done with courses at 10AM to meet you at 4:30 PM because it works for you.

Create a Slack Team for your team. Name it appropriately using your team name.
There is no need to invite Scot to your Slack Team. Dr. Morgan may want to be invited;
please ask her at your first meeting if she is you advisor.
Create a Bitbucket repository for your team (NOT GitHub).
This will be the repository for everything for your team for the next 22 weeks!
Name it appropriately with your team name. The owner of this repository will have additional duties and
management oversight of your team's documents and code.
It is probably a good idea for this person to be the same person you might envision as your Team Lead,
or the person with the most Git experience. Add a contributors file and a README that describe your team and this project.
Have all team members (and your advisor) fork the repository and then initialize a local copy (i.e. clone their own fork).
Lastly, come up with at least 3 candidate project ideas for your team project. For each one,
write a one sentence (or very short paragraph) description. Be prepared to discuss these in more depth at the meeting with your advisor.
Here are some high-level requirements for your team projects:
Must be based on the architecture we learned last term: ASP.NET MVC 5 + SQL Server, both deployed to Azure.
You are encouraged to add other resources to this as needed. Some examples:
other Azure or AWS services (i.e. file storage), email (SendGrid), SMS (Twilio).
Must be something interesting and useful; it should not be boring or routine.
Must use an external API for at least one major feature.
By external API I mean something like: Lyft, Twitter, Instagram, Google, YouTube, US Government.
And I don't mean something as simple as dropping a Google Map element on your page and calling it good.
I really hope one or more groups uses one of these (please, please?):
IBM Watson, Microsoft Azure Cognitive Services, Google Cloud AI, Amazon Machine Learning on AWS.
At least one major feature must involve an “algorithmic component” developed by your group.
It's OK to take on a higher risk project.
Personally I think it's better to try to do something more interesting and potentially more difficult and accomplish less,
than to have a really polished project that's boring and easy to do. That being said, you need to manage the risk.
If your project depends very heavily on X, then you better put a whole bunch of time into
learning and figuring out X during the inception phase. If depending on X forms part of your architecture,
you'd better prove it early as the book says.
