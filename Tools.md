# Tools, Configurations, and Packages #

## Microsoft Visual Studio Community 2017 ##

Version 15.9.6
VisualStudio.15.Release/15.9.6+28307.344
Microsoft .NET Framework
Version 4.7.03056

Installed Version: Community

### Application Insights Tools for Visual Studio Package ### 
8.14.11009.1
### Application Insights Tools for Visual Studio ###

### ASP.NET and Web Tools 2017 ###  
15.9.04012.0

### ASP.NET Core Razor Language Services ###   
15.8.31590
Provides languages services for ASP.NET Core Razor.

### ASP.NET Web Frameworks and Tools 2017 ### 
5.2.60913.0
For additional information, visit https://www.asp.net/

### Azure App Service Tools v3.0.0 ### 
15.9.03024.0

### Azure Data Lake Node ###
1.0
This package contains the Data Lake integration nodes for Server Explorer.

### Azure Data Lake Tools for Visual Studio ###
2.3.3000.2
Microsoft Azure Data Lake Tools for Visual Studio

### Azure Functions and Web Jobs Tools ###
15.9.02046.0

### Azure Stream Analytics Tools for Visual Studio ###
2.3.3000.2
Microsoft Azure Stream Analytics Tools for Visual Studio

### C# Tools ###
2.10.0-beta2-63501-03+b9fb1610c87cccc8ceb74a770dba261a58e39c4a
### C# components used in the IDE. ### 
Depending on your project type and settings, a different version of the compiler may be used.

### Common Azure Tools ###
1.10
Provides common services for use by Azure Mobile Services and Microsoft Azure Tools.

###Fabric.DiagnosticEvents ###
1.0
Fabric Diagnostic Events

### JavaScript Language Service ###
2.0
JavaScript Language Service

### JavaScript Project System ###  
2.0
JavaScript Project System

### Microsoft Azure HDInsight Azure Node ###
2.3.3000.2
HDInsight Node under Azure Node

### Microsoft Azure Hive Query Language Service ###
2.3.3000.2
Language service for Hive query

### Microsoft Azure Service Fabric Tools for Visual Studio ###
2.4
Microsoft Azure Service Fabric Tools for Visual Studio

### Microsoft Azure Stream Analytics Language Service ###
2.3.3000.2
Language service for Azure Stream Analytics

### Microsoft Azure Stream Analytics Node ###
1.0
Azure Stream Analytics Node under Azure Node

### Microsoft Azure Tools ###
2.9
Microsoft Azure Tools for Microsoft Visual Studio 2017 - v2.9.10730.2

### Microsoft Continuous Delivery Tools for Visual Studio ###
0.4
Simplifying the configuration of Azure DevOps pipelines from within the Visual Studio IDE.

### Microsoft JVM Debugger ###
1.0
Provides support for connecting the Visual Studio debugger to JDWP compatible Java Virtual Machines

### Microsoft Library Manager ### 
1.0
Install client-side libraries easily to any web project

### Microsoft MI-Based Debugger ###
1.0
Provides support for connecting Visual Studio to MI compatible debuggers

### Microsoft Visual Studio Tools for Containers ###
1.1
Develop, run, validate your ASP.NET Core applications in the target environment. F5 your application directly into a container with debugging, or CTRL + F5 to edit & refresh your app without having to rebuild the container.

### Node.js Tools ###
1.4.21001.1 Commit Hash:8dd15923800d931b153ab9e4de74e42a74eba5e6
Adds support for developing and debugging Node.js apps in Visual Studio

### NuGet Package Manager ###
4.6.0
NuGet Package Manager in Visual Studio. For more information about NuGet, visit http://docs.nuget.org/.

### ProjectServicesPackage Extension ###
1.0
ProjectServicesPackage Visual Studio Extension Detailed Info

### ResourcePackage Extension ###
1.0
ResourcePackage Visual Studio Extension Detailed Info

### ResourcePackage Extension ###
1.0
ResourcePackage Visual Studio Extension Detailed Info

### SQL Server Data Tools ###
15.1.61901.03220
Microsoft SQL Server Data Tools

### ToolWindowHostedEditor ###
1.0
Hosting json editor into a tool window

### TypeScript Tools ###
15.9.20918.2001
TypeScript Tools for Microsoft Visual Studio

### Visual Basic Tools ###
2.10.0-beta2-63501-03+b9fb1610c87cccc8ceb74a770dba261a58e39c4a
Visual Basic components used in the IDE. Depending on your project type and settings, a different version of the compiler may be used.

### Visual F# Tools 10.2 for F# 4.5 ###
15.8.0.0.  Commit Hash: 6e26c5bacc8c4201e962f5bdde0a177f82f88691.
Microsoft Visual F# Tools 10.2 for F# 4.5

### Visual Studio Code Debug Adapter Host Package ###
1.0
Interop layer for hosting Visual Studio Code debug adapters in Visual Studio

### Visual Studio Tools for Containers ###   
1.0
Visual Studio Tools for Containers
---------------------------------------------------------------------------------------------------------------
## Packages ##

### Entity Framework ###
Version: 6.2.0

### Antlr ###
Version: 3.5.0.2

### System.Web.Mvc ###
Version: 5.2.4.0

### bootstrap ###
Version: 3.3.7

### jQuery ###
Version: 3.3.1

### jQuery.Validation ###
Version: 1.17.0

### Identity.Core ###
Version: 2.2.2

### Identity.EntityFramework ###
Version: 2.2.2

### Identity.Owin ###
Version: 2.2.2

### AspNet.Razor ###
Version: 3.2.4

### AspNet.Web.Optimization ###
Version: 1.1.3

### AspNet.WebPages ###
Version: 3.2.4

### CodeDom.Providers.DotNetCompilerPlatform ###
Version: 2.0.0

### jQuery.Unobtrusive.Validation ###
Version: 3.2.11

### Microsoft.Owin ###
Version: 3.0.1

### Microsoft.Owin.Host.SystemWeb ###
Version: 4.0.1

### Microsoft.Owin.Security ###
Version: 4.0.1

### Microsoft.Owin.Security.Cookies ###
Version: 4.0.1

### Microsoft.Owin.Security.Facebook ###
Version: 4.0.1

### Microsoft.Owin.Security.Google ###
Version: 4.0.1

### Microsoft.Owin.Security.MicrosoftAccount ###
Version: 4.0.1

### Microsoft.Owin.Security.OAuth ###
Version: 4.0.1

### Microsoft.Owin.Security.Twitter ###
Version: 4.0.1

### Microsoft.Web.Infrastructure ###
Version: 1.0.0.0

### Modernizr ###
Version: 2.8.3

### Newtonsoft.Json ###
Version: 12.0.1

### Owin ###
Version: 4.0.1

### WebGrease ###
Version: 1.6.0
